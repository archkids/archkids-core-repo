#!/usr/bin/env bash
#
# Script name: build-db.sh
# Description: Script for rebuilding the database for archkids-core-repo.
# GitLab: https://gitlab.com/archkids/archkids-core-repo
# Contributors: Tipo Strano

# Set with the flags "-e", "-u", "-o pipefail" cause the script to fail
# if certain things happen, which is a good thing. Otherwise, we can
# get hidden bugs that are hard to discover.
set -euo pipefail

x86_pkgbuild=$(find ../archkids-pkgbuild/x86_64 -type f -name "*.pkg.tar.zst*")

for x in ${x86_pkgbuild}
do
    mv "${x}" x86_64/
    echo "Moving ${x}"
done

echo "###########################"
echo "Building the repo database."
echo "###########################"

## Arch: x86_64
cd x86_64
rm -f archkids-core-repo*

echo "###################################"
echo "Building for architecture 'x86_64'."
echo "###################################"

## repo-add
## -s: signs the packages
## -n: only add new packages not already in database
## -R: remove old package files when updating their entry
repo-add -n -R archkids-core-repo.db.tar.gz *.pkg.tar.zst

# Removing the symlinks because GitLab can't handle them.
rm archkids-core-repo.db
# rm archkids-core-repo.db.sig
rm archkids-core-repo.files
# rm archkids-core-repo.files.sig

# Renaming the tar.gz files without the extension.
mv archkids-core-repo.db.tar.gz archkids-core-repo.db
# mv archkids-core-repo.db.tar.gz.sig archkids-core-repo-db.sig
mv archkids-core-repo.files.tar.gz archkids-core-repo.files
# mv archkids-core-repo.files.tar.gz.sig archkids-core-repo.files.sig

echo "#######################################"
echo "Packages in the repo have been updated!"
echo "#######################################"